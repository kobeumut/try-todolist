package com.huawei.projecttracker.core;


import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;

import com.huawei.projecttracker.db.AppDatabase;


public abstract class BaseActivity<N extends BaseNavigator, T extends ViewDataBinding,
        VM extends BaseViewModel<N>> extends AppCompatActivity
        implements LifecycleOwner, BaseNavigator {

    public T binding;
    private LifecycleRegistry lifecycleRegistry;
    private VM viewModel;

    public abstract VM getViewModel();
    public AppDatabase database;

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
        }
        return lifecycleRegistry;
    }

    @LayoutRes
    protected abstract Integer getLayoutResId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lifecycleRegistry = new LifecycleRegistry(this);
        this.viewModel = viewModel == null ? getViewModel() : viewModel;
        viewModel.start((N) this);
        binding = DataBindingUtil.setContentView(this, getLayoutResId());
        database = AppDatabase.getDatabase(this);
        init(savedInstanceState);
    }

    protected abstract void init(Bundle savedInstanceState);


}
