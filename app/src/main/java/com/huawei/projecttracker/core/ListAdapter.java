package com.huawei.projecttracker.core;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.huawei.projecttracker.R;
import com.huawei.projecttracker.navigator.MainNavigator;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    public List list;
    public MainNavigator navigator;

    public ListAdapter(List list, MainNavigator navigator) {
        this.list = list;
        this.navigator = navigator;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_main, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public ImageButton firstButton;
        public ImageButton secondButton;
        public View cardView;

        public ListViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            textView = itemView.findViewById(R.id.tv);
            firstButton = itemView.findViewById(R.id.delete_button);
            secondButton = itemView.findViewById(R.id.edit_button);
        }
    }
}
