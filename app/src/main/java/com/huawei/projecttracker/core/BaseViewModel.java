package com.huawei.projecttracker.core;

import androidx.lifecycle.ViewModel;

public abstract class BaseViewModel<N extends BaseNavigator> extends ViewModel {
    protected N navigator;


    void start(N navigator) {
        if (this.navigator == null) {
            this.navigator = navigator;
        }
    }


}
