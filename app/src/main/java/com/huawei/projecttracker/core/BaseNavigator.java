package com.huawei.projecttracker.core;

public interface BaseNavigator {
    void showProgress();
    void hideProgress();
}
