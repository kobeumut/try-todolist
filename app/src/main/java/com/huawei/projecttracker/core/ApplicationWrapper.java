package com.huawei.projecttracker.core;


import android.app.Application;

public class ApplicationWrapper extends Application {

    private static ApplicationWrapper instance;

    public static synchronized ApplicationWrapper getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }


}
