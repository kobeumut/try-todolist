package com.huawei.projecttracker.ui.login;

import com.huawei.projecttracker.db.entity.UserEntity;

enum LoginResult {
    SUCCESS, FAILED;

    LoginResult() {

    }

    public static UserEntity SUCCESS(UserEntity userEntity) {
        return userEntity;
    }
}