package com.huawei.projecttracker.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.huawei.projecttracker.R;
import com.huawei.projecttracker.core.ListAdapter;
import com.huawei.projecttracker.db.entity.ItemEntity;
import com.huawei.projecttracker.navigator.MainNavigator;

import java.util.List;

public class ItemListAdapter extends ListAdapter {


    public ItemListAdapter(List<ItemEntity> list, MainNavigator navigator) {
        super(list, navigator);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        List<ItemEntity> itemList = (List<ItemEntity>) list;

        holder.textView.setText(itemList.get(position).getTitle());
        holder.firstButton.setOnClickListener(v ->
        {
            if (itemList.size() > 0) {
                ItemEntity selectedItem = itemList.get(position);
//                navigator.markDoneItem(selectedItem);
            }
        });
        holder.secondButton.setOnClickListener(v -> {
            if (itemList.size() > 0) {
                ItemEntity selectedItem = itemList.get(position);
//                navigator.editItem(selectedItem);
            }

        });
    }
    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_main, parent, false);
        return new ItemListViewHolder(view);
    }

    public class ItemListViewHolder extends ListViewHolder {

        public ItemListViewHolder(View itemView) {
            super(itemView);
            firstButton.setImageResource(android.R.drawable.checkbox_on_background);
        }
    }

}
