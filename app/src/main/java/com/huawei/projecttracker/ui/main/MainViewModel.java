package com.huawei.projecttracker.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.huawei.projecttracker.core.BaseViewModel;
import com.huawei.projecttracker.navigator.MainNavigator;

import java.util.List;


public class MainViewModel extends BaseViewModel<MainNavigator> {

    public MainViewModel() {

    }

    public void changeOverToItem() {

    }
    private MutableLiveData<List<String>> users;

    public LiveData<List<String>> getUsers() {
        if (users == null) {
            users = new MutableLiveData<List<String>>();
//            loadUsers();
        }
        return users;
    }
}
