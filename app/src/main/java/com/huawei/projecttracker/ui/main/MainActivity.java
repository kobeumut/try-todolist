package com.huawei.projecttracker.ui.main;

import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huawei.projecttracker.R;
import com.huawei.projecttracker.core.BaseActivity;
import com.huawei.projecttracker.core.ListAdapter;
import com.huawei.projecttracker.databinding.ActivityMainBinding;
import com.huawei.projecttracker.db.dao.ItemDao;
import com.huawei.projecttracker.db.dao.UserDao;
import com.huawei.projecttracker.db.entity.ItemEntity;
import com.huawei.projecttracker.db.entity.Status;
import com.huawei.projecttracker.db.entity.TodoListEntity;
import com.huawei.projecttracker.helper.CreateOrUpdateModel;
import com.huawei.projecttracker.navigator.MainNavigator;
import com.huawei.projecttracker.ui.adapter.ItemListAdapter;
import com.huawei.projecttracker.ui.adapter.TodoListAdapter;

import java.sql.Date;

public class MainActivity extends BaseActivity<MainNavigator, ActivityMainBinding, MainViewModel> implements MainNavigator {

    private TodoListEntity todo;
    private ItemEntity item;
    private Integer userId;
    private CreateOrUpdateModel viewType;
    private Integer todoListId;

    @Override
    public MainViewModel getViewModel() {
        return ViewModelProviders.of(this, null).get(MainViewModel.class);
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userId = getIntent().getIntExtra("user_id", 1);
        fetchTodos();
        binding.fab.setOnClickListener(view -> createTodo());
    }

    private void fetchTodos() {
        final UserDao userDao = database.userDao();
        userDao.allTodosOfUser(userId).observe(MainActivity.this, todosOfUser -> {
            viewType = CreateOrUpdateModel.CREATE_TODO;
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
            binding.recyclerView.setLayoutManager(layoutManager);
            TodoListAdapter todoListAdapter = new TodoListAdapter(todosOfUser.todos, MainActivity.this);
            setData(todoListAdapter);
        });
    }

    private void setData(ListAdapter adapter) {
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void deleteTodo(TodoListEntity todoListEntity) {
        database.todoDao().deleteItem(todoListEntity);
    }

    @Override
    public void updateTodo(TodoListEntity todoListEntity) {
        todo = todoListEntity;
        showDialog(CreateOrUpdateModel.UPDATE_TODO);
    }

    @Override
    public void createTodo() {
        showDialog(viewType);
    }

    @Override
    public void fetchItems(Integer todoId) {

        todoListId = todoId;
        final ItemDao itemDao = database.itemDao();
        itemDao.loadAllItems(todoId).observe(this, itemEntities -> {
            viewType = CreateOrUpdateModel.CREATE_ITEM;
            ItemListAdapter listAdapter = new ItemListAdapter(itemEntities, MainActivity.this);
            setData(listAdapter);
        });
    }

    private void showDialog(CreateOrUpdateModel cu) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((cu == CreateOrUpdateModel.CREATE_TODO) ? "Add Item" : "Edit Item");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", (dialog, which) -> {
            switch (cu) {
                case CREATE_TODO:
                    todo = new TodoListEntity(0, input.getText().toString(), userId);
                    database.todoDao().createItem(todo);
                    break;
                case UPDATE_TODO:
                    todo.setName(input.getText().toString());
                    database.todoDao().updateItem(todo);
                    break;
                case CREATE_ITEM:
                    long ctm = System.currentTimeMillis();
                    Date d = new Date(ctm);
                    item = new ItemEntity(0, input.getText().toString(), "", todoListId, Status.NOT_COMPLETE, d);
                    database.itemDao().insert(item);
                    break;

            }

        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }
    @Override
    public void onBackPressed() {
        if(viewType==CreateOrUpdateModel.CREATE_ITEM){
            fetchTodos();
        }else{
            super.onBackPressed();
        }
    }
}
