package com.huawei.projecttracker.ui.adapter;

import androidx.annotation.NonNull;

import com.huawei.projecttracker.core.ListAdapter;
import com.huawei.projecttracker.db.entity.TodoListEntity;
import com.huawei.projecttracker.navigator.MainNavigator;

import java.util.List;

public class TodoListAdapter extends ListAdapter {


    public TodoListAdapter(List<TodoListEntity> list, MainNavigator navigator) {
        super(list, navigator);
    }


    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        List<TodoListEntity> todoList = (List<TodoListEntity>) list;

        holder.textView.setText(todoList.get(position).getName());
        holder.firstButton.setOnClickListener(v ->
        {
            TodoListEntity selectedTodo = todoList.get(position);
            navigator.deleteTodo(selectedTodo);
        });
        holder.secondButton.setOnClickListener(v -> {
            if (todoList.size() > 0) {
                TodoListEntity selectedTodo = todoList.get(position);
                navigator.updateTodo(selectedTodo);
            }

        });
        holder.cardView.setOnClickListener(v->{
            if (todoList.size() > 0) {
                TodoListEntity selectedTodo = todoList.get(position);
                navigator.fetchItems(selectedTodo.getTodoListId());
            }
        });
    }
}
