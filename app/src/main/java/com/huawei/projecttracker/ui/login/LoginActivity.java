package com.huawei.projecttracker.ui.login;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.huawei.projecttracker.ui.main.MainActivity;
import com.huawei.projecttracker.R;
import com.huawei.projecttracker.core.BaseActivity;
import com.huawei.projecttracker.databinding.ActivityLoginBinding;
import com.huawei.projecttracker.db.entity.UserEntity;
import com.huawei.projecttracker.navigator.LoginNavigator;

public class LoginActivity extends BaseActivity<LoginNavigator, ActivityLoginBinding, LoginViewModel> {


    @Override
    public LoginViewModel getViewModel() {
        return ViewModelProviders.of(this, null)
                .get(LoginViewModel.class);
    }

    @Override
    protected Integer getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        getViewModel().getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                binding.login.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    binding.username.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    binding.password.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                getViewModel().loginDataChanged(binding.username.getText().toString(),
                        binding.password.getText().toString());
            }
        };
        binding.username.addTextChangedListener(afterTextChangedListener);
        binding.password.addTextChangedListener(afterTextChangedListener);
        binding.password.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    getViewModel().login(binding.username.getText().toString(),
                            binding.password.getText().toString());
                }
                return false;
            }
        });
        database.userDao().loadAllUsers().observe(LoginActivity.this, userEntities -> Log.e("",""));
        binding.register.setOnClickListener(view -> {
            showProgress();
            long id = database.userDao().insert(new UserEntity(0,binding.username.getText().toString(),
                    binding.password.getText().toString()));
            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            intent.putExtra("user_id",Integer.valueOf(id+""));
            startActivity(intent);
            finish();

        });

        binding.login.setOnClickListener(v -> {
            showProgress();
            database.userDao().login(binding.username.getText().toString(),
                    binding.password.getText().toString()).observe(LoginActivity.this, userEntity -> {
                        if(userEntity!=null){
                            Toast.makeText(LoginActivity.this,"Logged in",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                            intent.putExtra("user_id",userEntity.getId());
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(LoginActivity.this,getString(R.string.login_error),Toast.LENGTH_SHORT).show();
                            hideProgress();
                        }
                    });
        });
    }

    @Override
    public void showProgress() {
        binding.loading.setVisibility(View.VISIBLE);
    }
    @Override
    public void hideProgress() {
        binding.loading.setVisibility(View.GONE);
    }
}
