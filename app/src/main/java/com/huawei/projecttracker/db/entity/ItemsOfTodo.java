package com.huawei.projecttracker.db.entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ItemsOfTodo {
    @Embedded
    public TodoListEntity todo;

    @Relation(parentColumn = "id", entityColumn = "list_id", entity = ItemEntity.class)
    public List<ItemEntity> items;
}
