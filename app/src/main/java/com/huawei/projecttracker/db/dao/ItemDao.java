package com.huawei.projecttracker.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.huawei.projecttracker.db.entity.ItemEntity;

import java.util.List;

@Dao
public interface ItemDao {
    //TODO: Duzelt
    @Query("SELECT * FROM item where list_id = :todoId")
    LiveData<List<ItemEntity>> loadAllItems(Integer todoId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ItemEntity> items);

    @Query("SELECT * FROM item")
    List<ItemEntity> list();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ItemEntity item);

}
