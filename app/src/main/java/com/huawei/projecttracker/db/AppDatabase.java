package com.huawei.projecttracker.db;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.huawei.projecttracker.db.converter.DateConverter;
import com.huawei.projecttracker.db.dao.ItemDao;
import com.huawei.projecttracker.db.dao.TodoListDao;
import com.huawei.projecttracker.db.dao.UserDao;
import com.huawei.projecttracker.db.entity.ItemEntity;
import com.huawei.projecttracker.db.entity.TodoListEntity;
import com.huawei.projecttracker.db.entity.UserEntity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Database(entities = {UserEntity.class, TodoListEntity.class, ItemEntity.class}, version = 1,exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;
    private static final String DB_NAME = "task.db";

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DB_NAME)
                            .allowMainThreadQueries() // SHOULD NOT BE USED IN PRODUCTION !!!
                            .addCallback(new RoomDatabase.Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Log.d("AppDatabase", "populating with data...");
                                    new PopulateDbAsync(INSTANCE).execute();
                                }
                            })
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public void clearDb() {
        if (INSTANCE != null) {
            new PopulateDbAsync(INSTANCE).execute();
        }
    }

    public abstract ItemDao itemDao();

    public abstract UserDao userDao();

    public abstract TodoListDao todoDao();

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final ItemDao itemDao;
        private final UserDao userDao;
        private final TodoListDao todoDao;

        public PopulateDbAsync(AppDatabase instance) {
            itemDao = instance.itemDao();
            userDao = instance.userDao();
            todoDao = instance.todoDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<UserEntity> users = new ArrayList<UserEntity>();
            users.add(new UserEntity(1,  "umutadali@gmail.com", "123123"));
            users.add(new UserEntity(2, "umutadali2@gmail.com", "123333"));
            userDao.insertAll(users);
            List<TodoListEntity> todos = new ArrayList<TodoListEntity>();
            todos.add(new TodoListEntity(1, "yeni liste", 1));
            todos.add(new TodoListEntity(2, "ikinci liste", 2));
            todoDao.insertAll(todos);
            List<ItemEntity> items = new ArrayList<ItemEntity>();
            long ctm=System.currentTimeMillis();
            Date d= new Date(ctm);
            items.add(new ItemEntity(1, "New Todo List", "Awesome", 1,0, d));
            items.add(new ItemEntity(1, "Shopping List", "Awesome", 2,0, d));
            itemDao.insertAll(items);
            return null;
        }
    }
}