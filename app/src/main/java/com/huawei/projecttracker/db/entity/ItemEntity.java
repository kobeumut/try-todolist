package com.huawei.projecttracker.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;

@Entity(tableName = "item")
public class ItemEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int itemId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "list_id")
    private int listId;

    @ColumnInfo(name = "status")
    private int status;

    @ColumnInfo(name = "deadline")
    private Date deadline;

    public ItemEntity(int itemId, String title, String description, int listId, int status, Date deadline) {
        this.itemId = itemId;
        this.title = title;
        this.description = description;
        this.listId = listId;
        this.status = status;
        this.deadline = deadline;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
}
