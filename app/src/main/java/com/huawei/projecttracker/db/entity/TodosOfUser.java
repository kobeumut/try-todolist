package com.huawei.projecttracker.db.entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class TodosOfUser {
    @Embedded
    public UserEntity user;

    @Relation(parentColumn = "id", entityColumn = "user_id", entity = TodoListEntity.class)
    public List<TodoListEntity> todos;
}
