package com.huawei.projecttracker.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.huawei.projecttracker.db.entity.TodosOfUser;
import com.huawei.projecttracker.db.entity.UserEntity;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    LiveData<List<UserEntity>> loadAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserEntity> users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(UserEntity user);

    @Query("SELECT * FROM user WHERE email = :email AND password = :password")
    LiveData<UserEntity> login(String email, String password);

    @Transaction
    @Query("SELECT * FROM user WHERE id = :userId")
    LiveData<TodosOfUser> allTodosOfUser(long userId);


}
