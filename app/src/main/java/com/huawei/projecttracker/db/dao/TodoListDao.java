package com.huawei.projecttracker.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.huawei.projecttracker.db.entity.ItemsOfTodo;
import com.huawei.projecttracker.db.entity.TodoListEntity;

import java.util.List;

@Dao
public interface TodoListDao {
    @Query("SELECT * FROM todo")
    LiveData<List<TodoListEntity>> loadAllTodos();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TodoListEntity> todos);

    @Transaction
    @Query("SELECT * FROM todo WHERE id = :id")
    ItemsOfTodo itemOfTodo(long id);

    @Update
    void updateItem(TodoListEntity todoListEntity);

    @Insert
    void createItem(TodoListEntity todoListEntity);

    @Delete
    void deleteItem(TodoListEntity todoListEntity);

}
