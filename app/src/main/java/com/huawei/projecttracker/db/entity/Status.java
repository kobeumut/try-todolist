package com.huawei.projecttracker.db.entity;

public class Status {
    public static final int COMPLETE = 1;
    public static final int NOT_COMPLETE= 0;
    public static final int EXPIRED = 2;
}
