package com.huawei.projecttracker.navigator;

import com.huawei.projecttracker.core.BaseNavigator;
import com.huawei.projecttracker.db.entity.TodoListEntity;

public interface MainNavigator extends BaseNavigator {
    void deleteTodo(TodoListEntity todoListEntity);
    void updateTodo(TodoListEntity todoListEntity);
    void createTodo();

    void fetchItems(Integer todoId);
}
