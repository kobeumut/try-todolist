package com.huawei.projecttracker.navigator;

import com.huawei.projecttracker.core.BaseNavigator;

public interface LoginNavigator extends BaseNavigator {
    void checkLogin(String mail, String password);
}
