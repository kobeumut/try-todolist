package com.huawei.projecttracker.helper;

public enum CreateOrUpdateModel {
    CREATE_TODO, UPDATE_TODO, CREATE_ITEM, UPDATE_ITEM
}
